﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class AlphaControler : MonoBehaviour 
{	
	public float m_InitAlpha = 0;
	public float m_FadeInTime = 1;
	private Color m_Color;

	// Use this for initialization
	void Start () 
	{
		m_Color = GetComponent<Image>().color;
		m_Color.a = m_InitAlpha;
		GetComponent<Image>().color = m_Color;
	}
	
	// Update is called once per frame
	void Update () 
	{
		if(m_Color.a < 1f)
		{
			if(m_FadeInTime != 0)
			{
				m_Color.a += Time.deltaTime* (1/m_FadeInTime);
				GetComponent<Image>().color = m_Color;
			}		
		}
	}

	public bool SplashDone()
	{
		return (m_Color.a >= 1);
	}
}
