﻿using UnityEngine;
using System.Collections;

public class Panel : MonoBehaviour 
{
	public GameObject m_ReadyPanel;
	public GameObject m_ActivatedPanel;
	public GameObject m_SlotPanel;
	public int m_PanelPosition = 0;

	// Use this for initialization
	void Start () 
	{
		
	}
	
	// Update is called once per frame
	void Update () 
	{
	
	}

	public void ResetPanel()
	{
		m_ActivatedPanel.SetActive(false);
		    m_ReadyPanel.SetActive(false);
		     m_SlotPanel.SetActive(false);
		MovePanelToZero();
	}

	public bool ActivatedPanel
	{
		get{return m_ActivatedPanel.activeSelf;}
		set
		{
			
			m_ActivatedPanel.SetActive(value);
			m_SlotPanel.SetActive(value);
			if(!value)
			{
				MovePanelToZero();
			}
		}
	}

	public bool ReadyPanel
	{
		get{return m_ReadyPanel.activeSelf;}
		set{m_ReadyPanel.SetActive(value);}
	}

	void MovePanelToZero()
	{
		Vector3 pos = GetComponent<RectTransform>().localPosition;
		pos.x = 0;
		GetComponent<RectTransform>().localPosition = pos;
	}






}
