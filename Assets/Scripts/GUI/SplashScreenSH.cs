﻿using UnityEngine;
using System.Collections;

public class SplashScreenSH : MonoBehaviour 
{
	public AlphaControler m_AlphaControler;
	public GameObject m_Menu;

	// Use this for initialization
	void Start () 
	{
	
	}
	
	// Update is called once per frame
	void Update () 
	{
		if(m_Menu && m_AlphaControler.SplashDone())
		{
			Instantiate(m_Menu);
			Destroy(this);
		}
	}

	public bool SplashScreenDone()
	{
		return m_AlphaControler.SplashDone();
	}

}
