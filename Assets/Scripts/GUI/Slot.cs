﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class Slot : MonoBehaviour 
{
	public List<Sprite> m_ItemInSlot;
	public Image m_ImageScript;
	public int m_SlotPosition = 0;


	// Use this for initialization
	void Start () 
	{
	
	}
	
	// Update is called once per frame
	void Update () 
	{
	
	}

	public void SwitchSlot(int _newSlot)
	{
		m_ImageScript.sprite = m_ItemInSlot[_newSlot];
		m_SlotPosition = _newSlot;
	}

}
