﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


public class ColorSelect : MonoBehaviour 
{

	public List<Panel> m_PlayerPanelList;
	public int m_PlayerLeader;
	public ColorSelectedList m_SelectedColor;
	public int m_NumOfReadyPlayer = 0;
	public int m_NumOfActivePlayer = 0;


	// Use this for initialization
	void Start () 
	{
		m_SelectedColor = new ColorSelectedList(m_PlayerPanelList.Count);
	}
	
	// Update is called once per frame
	void Update () 
	{	
		UpdatePanelsPosition();
		UpdateSlot();
		UpdatePlayerState();

	}

	void UpdatePlayerState()
	{
		int actPla = 0;
		int redPla = 0;
		for (int i = 0; i < m_PlayerPanelList.Count; i++) 
		{
			if(m_PlayerPanelList[i].ActivatedPanel)
			{
				actPla++;
			}
			if(m_PlayerPanelList[i].ReadyPanel)
			{
				redPla++;
			}
		}
		m_NumOfReadyPlayer = redPla;
		m_NumOfActivePlayer = actPla;
	}

	public bool CheckIfReady()
	{
		return (m_NumOfActivePlayer == m_NumOfReadyPlayer && m_NumOfReadyPlayer > 1);
	}

	public void UpdateSlot()
	{
		for(int i = 0; i < m_PlayerPanelList.Count; i++)
		{			
			if(m_SelectedColor.colorList[m_PlayerPanelList[i].m_SlotPanel.GetComponent<Slot>().m_SlotPosition] == true && m_PlayerPanelList[i].ReadyPanel == false)
			{
				SwitchSlot("right", i);
			}
		}
	}

	public void ResetPlayerPanel()
	{
		
		for(int i = 0; i < m_PlayerPanelList.Count; i++)
		{
			m_PlayerPanelList[i].ResetPanel();
		}
	}


	public int GetAvailableSLot(string _dir, int _pos)
	{
		int index = 0;

		if(_dir == "right")
		{
			if(_pos == m_SelectedColor.colorList.Length-1)
			{
				if(m_SelectedColor.colorList[0] == false)
				{
					index = 0;				
				}
				else
				{
					index = GetAvailableSLot(_dir, 0);
				}
			}
			else
			{
				if(m_SelectedColor.colorList[_pos+1] == false)
				{
					index = _pos+1;
				}
				else
				{
					index = GetAvailableSLot(_dir, _pos+1);
				}
			}
		}
		else if(_dir == "left")
		{
			if(_pos == 0)
			{
				if(m_SelectedColor.colorList[m_SelectedColor.colorList.Length-1] == false)
				{
					index = m_SelectedColor.colorList.Length-1;				
				}
				else
				{
					index = GetAvailableSLot(_dir, m_SelectedColor.colorList.Length-1);
				}
			}
			else
			{
				if(m_SelectedColor.colorList[_pos-1] == false)
				{
					index = _pos-1;
				}
				else
				{
					index = GetAvailableSLot(_dir, _pos-1);
				}
			}
		}

		return index;
	}



	public void SetPartyLeader(int _set)
	{
		m_PlayerLeader = _set;
		ActivatePlayer(m_PlayerLeader);
		
	}


	public int PlayerLeader
	{
		get{return m_PlayerLeader;}
		set{m_PlayerLeader = value;}
	}


	public void ActivatePlayer(int _player_id)
	{			
		m_PlayerPanelList[_player_id].ActivatedPanel = true;

	}

	public void ReadyPlayer(int _player_id)
	{
		m_PlayerPanelList[_player_id].ReadyPanel = true;
	}

	public int NumberOfActivatedPlayer()
	{
		int num = 0;
		for(int i = 0; i < m_PlayerPanelList.Count; i++)
		{
			if(m_PlayerPanelList[i].ActivatedPanel)
			{
				num++;
			}
		}
		return num;
	}

	public void LockColor(int _player_id)
	{
		if(m_SelectedColor != null)
		{
			m_SelectedColor.colorList[m_PlayerPanelList[_player_id].m_SlotPanel.GetComponent<Slot>().m_SlotPosition] = true;
		}
	}

	public void UnLockColor(int _player_id)
	{
		if(m_SelectedColor != null)
		{
			m_SelectedColor.colorList[m_PlayerPanelList[_player_id].m_SlotPanel.GetComponent<Slot>().m_SlotPosition] = false;
		}
	}

	public void UnLockAllColor()
	{
		for (int i = 0; i < m_SelectedColor.colorList.Length; i++) 
		{
			m_SelectedColor.colorList[i] = false;
		}

		for (int i = 0; i < m_PlayerPanelList.Count; i++) 
		{			
			m_PlayerPanelList[i].m_SlotPanel.GetComponent<Slot>().SwitchSlot(0);
		}
	}


	void UpdatePanelsPosition()
	{
		if(NumberOfActivatedPlayer() == 1)
		{
			for(int i = 0; i < m_PlayerPanelList.Count; i++)
			{
				if(m_PlayerPanelList[i].ActivatedPanel)
				{					
					if(m_PlayerPanelList[i].GetComponent<RectTransform>().localPosition.x != 0)
					{						
						//get sign for direction to move
						float sign = Mathf.Sign(m_PlayerPanelList[i].GetComponent<RectTransform>().localPosition.x);

						//move the panel
						m_PlayerPanelList[i].GetComponent<RectTransform>().localPosition += new Vector3 (5*-sign,0,0);

						//check if overlaped
						bool overlap = false;
						if(sign != Mathf.Sign(m_PlayerPanelList[i].GetComponent<RectTransform>().localPosition.x))
						{								
							overlap = true;
						}

						//set X to 0 if overlaped
						if(overlap)
						{
							Vector3 rect = m_PlayerPanelList[i].GetComponent<RectTransform>().localPosition;
							rect.x = 0;
							m_PlayerPanelList[i].GetComponent<RectTransform>().localPosition = rect;
						}
					}
					break;
				}
			}
		}
		else if(NumberOfActivatedPlayer() > 1)
		{
			int panel_order = 0;
			//float panel_width = m_PlayerPanelList[0].GetComponent<RectTransform>().rect.width;
			float panel_width = 160;

			//give position to each active panel
			for(int i = 0; i < m_PlayerPanelList.Count; i++)
			{
				if(m_PlayerPanelList[i].ActivatedPanel)
				{
					m_PlayerPanelList[i].m_PanelPosition = panel_order;
					panel_order++;
				}
			}


			float initPos = -1 * (panel_width/2) * (NumberOfActivatedPlayer() - 1);
			int active_panel_cursor = 0;

			for(int i = 0 ; i < m_PlayerPanelList.Count; i++)
			{	
				if(m_PlayerPanelList[i].ActivatedPanel)
				{
					float panel_positionX = initPos + (panel_width * active_panel_cursor);

					if(m_PlayerPanelList[i].GetComponent<RectTransform>().localPosition.x != panel_positionX)
					{
						//get sign for direction to move
						float sign = Mathf.Sign(panel_positionX - m_PlayerPanelList[i].GetComponent<RectTransform>().localPosition.x);

						//move the panel
						m_PlayerPanelList[i].GetComponent<RectTransform>().localPosition += new Vector3 (5*sign,0,0);

						//check if overlaped
						bool overlap = false;
						if(sign != Mathf.Sign(panel_positionX - m_PlayerPanelList[i].GetComponent<RectTransform>().localPosition.x))
						{								
							overlap = true;
						}

						//set X to 0 if overlaped
						if(overlap)
						{
							Vector3 rect = m_PlayerPanelList[i].GetComponent<RectTransform>().localPosition;
							rect.x = panel_positionX;
							m_PlayerPanelList[i].GetComponent<RectTransform>().localPosition = rect;
						}
					}
					active_panel_cursor++;
				}
			}
		}
	}

	/// <summary>
	/// Switchs the slot.
	/// "left" for left, "right" for right
	/// </summary>
	/// <param name="_dir">Dir.</param>
	public void SwitchSlot(string _dir, int _player_id)
	{		

		int newSlot = 0;
				
		int pos = m_PlayerPanelList[_player_id].m_SlotPanel.GetComponent<Slot>().m_SlotPosition;

		newSlot = GetAvailableSLot(_dir, pos);

		m_PlayerPanelList[_player_id].m_SlotPanel.GetComponent<Slot>().SwitchSlot(newSlot);	

	}

	public class ColorSelectedList
	{
		public bool[] colorList;

		public ColorSelectedList(int _listLength)
		{
			InitList(_listLength);
		}

		public void InitList(int _listLength)
		{
			colorList = new bool[_listLength];
			for(int i = 0; i < _listLength; i++)
			{
				colorList[i] = false;
			}
		}

		public void ResetList()
		{
			for(int i = 0; i < colorList.Length; i++)
			{
				colorList[i] = false;
			}
		}
	}





}
