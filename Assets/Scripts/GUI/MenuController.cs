﻿using UnityEngine;
using System.Collections;

public class MenuController : MonoBehaviour 
{
	public GameObject m_Cursor;
	private int m_CursorPosition = 0;
	private Vector3 m_CursorOffset = new Vector3(-50,0,0);
	 
	public MenuWindow[] m_MenuWindow;
	private int m_MenuPositionCursor = 0;

	public GameObject m_ColorSelectObj;


	// Use this for initialization
	void Start () 
	{
		SetCursorPosition();
	}
	
	// Update is called once per frame
	void Update () 
	{		
		SetCursorPosition();
		PlayerControl();
	}

    bool CheckReady(ColorSelect cs, int index)
    {
        if (!cs.m_PlayerPanelList[index].ActivatedPanel)
        {
            cs.m_PlayerPanelList[index].ReadyPanel = true;
            cs.LockColor(index);

            cs.m_PlayerPanelList[index].ActivatedPanel = true;
            return true;
        }

        return false;
    }


	void PlayerControl()
	{
        ColorSelect cs = m_ColorSelectObj.GetComponent<ColorSelect>();
        for (int index = 0; index < GameManager.GetInstance().GetCurrentPlayerCount(); ++index)
        {
            Player player = GameManager.GetInstance().GetPlayerAt(index);
            if (player.Device.Action1)
            {
                if (CheckReady(cs, index))
                {
                    Debug.Log("GO:" + index);
                }
            }
        }

		if(GetUpInput())
		{
			CursorMoveUp();
			SetCursorPosition();
		}
		else if(GetDownInput())
		{
			CursorMoveDown();
			SetCursorPosition();
		}
		 
		if(m_MenuPositionCursor == 0) // if on main window
		{
			int playerDoingBAction = -1;

			if(Player1A())
			{
				playerDoingBAction = 0;
			}
			if(Player2A())
			{
				playerDoingBAction = 1;
			}
			if(Player3A())
			{
				playerDoingBAction = 2;
			}
			if(Player4A())
			{
				playerDoingBAction = 3;
			}

			if(playerDoingBAction != -1)
			{
				if(m_CursorPosition == 0) //on start game
				{							
					LoadColorSelectScreen(playerDoingBAction);
				}
				else if(m_CursorPosition == 1) //on leave game
				{				
					Application.Quit();
				}	
			}



		}
		else if(m_MenuPositionCursor == 1) //if on menu color select
		{
			//-1 if defaut for no one;
			int playerDoingBAction = -1;

			if(Player1B())
			{
				playerDoingBAction = 0;
			}
			if(Player2B())
			{
				playerDoingBAction = 1;
			}
			if(Player3B())
			{
				playerDoingBAction = 2;
			}
			if(Player4B())
			{
				playerDoingBAction = 3;
			}

			if(playerDoingBAction != -1)
			{
				if(m_MenuWindow[1].menuWindowObject.GetComponent<ColorSelect>().m_PlayerPanelList[playerDoingBAction].ReadyPanel)
				{
					m_MenuWindow[1].menuWindowObject.GetComponent<ColorSelect>().m_PlayerPanelList[playerDoingBAction].ReadyPanel = false;
					m_ColorSelectObj.GetComponent<ColorSelect>().UnLockColor(playerDoingBAction);
				}
				else if(m_MenuWindow[1].menuWindowObject.GetComponent<ColorSelect>().m_PlayerPanelList[playerDoingBAction].ActivatedPanel)
				{
					m_MenuWindow[1].menuWindowObject.GetComponent<ColorSelect>().m_PlayerPanelList[playerDoingBAction].ActivatedPanel = false;
					if(m_MenuWindow[1].menuWindowObject.GetComponent<ColorSelect>().PlayerLeader == playerDoingBAction)
					{
						m_ColorSelectObj.GetComponent<ColorSelect>().UnLockAllColor();
						LoadMainWindow();
					}
				}
			}
		}

		if(m_MenuPositionCursor == 1)
		{
			
			int playerDoingUpAction = -1;
			int playerDoingDownAction = -1;

			if(Player1Up())
			{				
				playerDoingUpAction = 0;
			}
			if(Player1Down())
			{				
				playerDoingDownAction = 0;
			}

			if(Player2Up())
			{
				playerDoingUpAction = 1;
			}
			if(Player2Down())
			{				
				playerDoingDownAction = 1;
			}

			if(Player3Up())
			{
				playerDoingUpAction = 2;
			}
			if(Player3Down())
			{				
				playerDoingDownAction = 2;
			}

			if(Player4Up())
			{
				playerDoingUpAction = 3;
			}
			if(Player4Down())
			{				
				playerDoingDownAction = 3;
			}

			if(playerDoingUpAction != -1)
			{
				if(m_MenuWindow[1].menuWindowObject.GetComponent<ColorSelect>().m_PlayerPanelList[playerDoingUpAction].ActivatedPanel && 
					!m_MenuWindow[1].menuWindowObject.GetComponent<ColorSelect>().m_PlayerPanelList[playerDoingUpAction].ReadyPanel)
				{					
					m_MenuWindow[1].menuWindowObject.GetComponent<ColorSelect>().SwitchSlot("left", playerDoingUpAction);
				}
			}

			if(playerDoingDownAction != -1)
			{
				if(m_MenuWindow[1].menuWindowObject.GetComponent<ColorSelect>().m_PlayerPanelList[playerDoingDownAction].ActivatedPanel && 
					!m_MenuWindow[1].menuWindowObject.GetComponent<ColorSelect>().m_PlayerPanelList[playerDoingDownAction].ReadyPanel)
				{					
					m_MenuWindow[1].menuWindowObject.GetComponent<ColorSelect>().SwitchSlot("right", playerDoingDownAction);
				}
			}


		}
	}

	void LoadColorSelectScreen(int party_leader)
	{		
		SwitchMenuCanvas(1, party_leader);
	}
	void LoadMainWindow()
	{
		SwitchMenuCanvas(0);
	}

	void SwitchMenuCanvas(int menu_index, int party_leader = -1)
	{				

		m_MenuWindow[1].menuWindowObject.GetComponent<ColorSelect>().ResetPlayerPanel();	

		if(m_MenuWindow[menu_index].menuWindowObject.GetComponent<ColorSelect>())			
		{
			
			m_MenuWindow[menu_index].menuWindowObject.GetComponent<ColorSelect>().SetPartyLeader(party_leader);
		}
	
		
		m_MenuWindow[m_MenuPositionCursor].isActive = false;
		m_MenuWindow[m_MenuPositionCursor].menuWindowObject.SetActive(false);
		m_MenuWindow[menu_index].isActive = true;
		m_MenuWindow[menu_index].menuWindowObject.SetActive(true);

		m_MenuPositionCursor = menu_index;
		m_CursorPosition = 0;
		SetCursorPosition();
	}



	void SetCursorPosition()
	{
		Vector3 cursor_pos = m_MenuWindow[m_MenuPositionCursor].button[m_CursorPosition].GetComponent<RectTransform>().position
			+ new Vector3(-(m_MenuWindow[m_MenuPositionCursor].button[m_CursorPosition].GetComponent<RectTransform>().rect.width)/2,0,0)
			+ m_CursorOffset; 

		m_Cursor.GetComponent<RectTransform>().position = cursor_pos;
	}

	bool GetUpInput()
	{
		bool upPressed = false;
		if(Input.GetKeyDown(KeyCode.UpArrow))
		{
			upPressed = true;
		}
		return upPressed;
	}

	bool GetDownInput()
	{
		bool downPressed = false;
		if(Input.GetKeyDown(KeyCode.DownArrow))
		{
			downPressed = true;
		}
		return downPressed;
	}

	void CursorMoveUp()
	{
		if(m_CursorPosition == GetActivatedMenu().button.Length-1)
		{
			m_CursorPosition = 0;
		}
		else
		{
			m_CursorPosition++;
		}
	}

	void CursorMoveDown()
	{
		if(m_CursorPosition == 0)
		{
			m_CursorPosition = GetActivatedMenu().button.Length-1;
		}
		else
		{
			m_CursorPosition--;
		}

	}

	bool Player1Up()
	{
		
		bool UpPressed = false;
		if(Input.GetKeyDown(KeyCode.UpArrow))
		{			
			UpPressed = true;
		}
		return UpPressed;
	}
	bool Player2Up()
	{
		bool UpPressed = false;
		if(Input.GetKeyDown(KeyCode.Q))
		{
			UpPressed = true;
		}
		return UpPressed;
	}
	bool Player3Up()
	{
		bool UpPressed = false;
		if(Input.GetKeyDown(KeyCode.A))
		{
			UpPressed = true;
		}
		return UpPressed;
	}
	bool Player4Up()
	{
		bool UpPressed = false;
		if(Input.GetKeyDown(KeyCode.Z))
		{
			UpPressed = true;
		}
		return UpPressed;
	}

	bool Player1Down()
	{
		bool downPressed = false;
		if(Input.GetKeyDown(KeyCode.DownArrow))
		{
			downPressed = true;
		}
		return downPressed;
	}
	bool Player2Down()
	{
		bool downPressed = false;
		if(Input.GetKeyDown(KeyCode.W))
		{
			downPressed = true;
		}
		return downPressed;
	}
	bool Player3Down()
	{
		bool downPressed = false;
		if(Input.GetKeyDown(KeyCode.S))
		{
			downPressed = true;
		}
		return downPressed;
	}
	bool Player4Down()
	{
		bool downPressed = false;
		if(Input.GetKeyDown(KeyCode.X))
		{
			downPressed = true;
		}
		return downPressed;
	}

	/// <summary>
	/// return true Player 1 press a.
	/// </summary>
	/// <returns><c>true</c>, if a was player1ed, <c>false</c> otherwise.</returns>
	bool Player1A()
	{
		
		bool aPressed = false;
		if(Input.GetKeyDown(KeyCode.Space))
		{			
			aPressed = true;
		}
		return aPressed;
	}
	/// <summary>
	/// return true Player 1 press a.
	/// </summary>
	bool Player2A()
	{
		bool aPressed = false;
		if(Input.GetKeyDown(KeyCode.E))
		{
			aPressed = true;
		}
		return aPressed;
	}
	/// <summary>
	/// return true Player 1 press a.
	/// </summary>
	bool Player3A()
	{
		bool aPressed = false;
		if(Input.GetKeyDown(KeyCode.D))
		{
			aPressed = true;
		}
		return aPressed;
	}
	/// <summary>
	/// return true Player 1 press a.
	/// </summary>
	bool Player4A()
	{
		bool aPressed = false;
		if(Input.GetKeyDown(KeyCode.C))
		{
			aPressed = true;
		}
		return aPressed;
	}

	/// <summary>
	/// return true Player 1 press a.
	/// </summary>
	/// <returns><c>true</c>, if a was player1ed, <c>false</c> otherwise.</returns>
	bool Player1B()
	{

		bool aPressed = false;
		if(Input.GetKeyDown(KeyCode.Escape))
		{			
			aPressed = true;
		}
		return aPressed;
	}
	/// <summary>
	/// return true Player 1 press a.
	/// </summary>
	bool Player2B()
	{
		bool aPressed = false;
		if(Input.GetKeyDown(KeyCode.R))
		{
			aPressed = true;
		}
		return aPressed;
	}
	/// <summary>
	/// return true Player 1 press a.
	/// </summary>
	bool Player3B()
	{
		bool aPressed = false;
		if(Input.GetKeyDown(KeyCode.F))
		{
			aPressed = true;
		}
		return aPressed;
	}
	/// <summary>
	/// return true Player 1 press a.
	/// </summary>
	bool Player4B()
	{
		bool aPressed = false;
		if(Input.GetKeyDown(KeyCode.V))
		{
			aPressed = true;
		}
		return aPressed;
	}

	MenuWindow GetActivatedMenu()
	{
		MenuWindow menu_window = new MenuWindow();

		for(int i = 0; i < m_MenuWindow.Length; i++)
		{
			if(m_MenuWindow[i].isActive)
			{
				menu_window = m_MenuWindow[i];
				break;
			}
		}

		return menu_window;
	}

	[System.Serializable]
	public class MenuWindow
	{
		public bool isActive;
		public GameObject menuWindowObject;
		public GameObject[] button;

		public MenuWindow()	{}

	}




}
