﻿using UnityEngine;
using System.Collections;

public class SpawnPoint : MonoBehaviour
{
    ////////////////////////////////////////////////////////////////////////////////
    // Members:
    ////////////////////////////////////////////////////////////////////////////////
    /*----------------------------------------------------------------------------*/
    public int m_InitialPlayerID;

    ////////////////////////////////////////////////////////////////////////////////
    // Callbacks:
    ////////////////////////////////////////////////////////////////////////////////
    /*----------------------------------------------------------------------------*/
    void Start()
    {
        if (m_InitialPlayerID < 0 || m_InitialPlayerID > GameManager.GetMaxPlayers()) {
            Debug.LogError("Invalid SpawnPoint initial player id!");
            m_InitialPlayerID = -1;
        }
    }

    #if UNITY_EDITOR
    /*----------------------------------------------------------------------------*/
    void OnDrawGizmos()
    {
        Gizmos.color = GetColorFromPlayerID(m_InitialPlayerID);
        Gizmos.DrawCube(transform.position, new Vector3(transform.localScale.x, transform.localScale.y, 0));
        Gizmos.color = Color.white;
    }
    #endif

    ////////////////////////////////////////////////////////////////////////////////
    // Privates:
    ////////////////////////////////////////////////////////////////////////////////
    /*----------------------------------------------------------------------------*/
    private Color GetColorFromPlayerID(int player_id)
    {
        switch (player_id)
        {
            case 0:
                return new Color(1.0f, 0.0f, 0.0f, 0.5f);
            case 1:
                return new Color(0.0f, 1.0f, 0.0f, 0.5f);
            case 2:
                return new Color(0.0f, 0.0f, 1.0f, 0.5f);
            case 3:
                return new Color(1.0f, 1.0f, 0.0f, 0.5f);
            default:
                Debug.LogError("Invalid player id!");
                break;
        }

        return Color.white;
    }
}