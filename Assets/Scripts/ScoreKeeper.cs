﻿using UnityEngine;
using System.Collections;

public class ScoreKeeper
{
	int m_NumberOfPlayer;
	int[] m_PlayerScoreList;


	public ScoreKeeper(int _numberOfPlayer)
	{
		m_NumberOfPlayer = _numberOfPlayer;
		m_PlayerScoreList = new int[m_NumberOfPlayer];
		Init();
	}

	void Init()
	{
		Reset();
	}

	public void AddScore(int _player)
	{
		m_PlayerScoreList[_player]++;
	}

	public void Reset()
	{
		for (int i = 0; i < m_NumberOfPlayer; i++)
		{
			m_PlayerScoreList[i] = 0;
		}
	}

	public int GetPlayerScore(int _player)
	{
		return m_PlayerScoreList[_player];
	}

			
}
