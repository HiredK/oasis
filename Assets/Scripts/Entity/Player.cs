﻿using UnityEngine;
using System.Collections;
using InControl;

public class Player : Entity
{
    ////////////////////////////////////////////////////////////////////////////////
    // Members:
    ////////////////////////////////////////////////////////////////////////////////
    /*----------------------------------------------------------------------------*/
    public InputDevice Device { get; set; }
    public int ID { get; set; }

    public GameObject m_LowerBody;
    private Vector2 m_LowerLookat = Vector2.down;

    public GameObject m_UpperBody;
    private Vector2 m_UpperLookat = Vector2.down;

    private Rigidbody2D m_Body;

    ////////////////////////////////////////////////////////////////////////////////
    // Callbacks:
    ////////////////////////////////////////////////////////////////////////////////
    /*----------------------------------------------------------------------------*/
    void Start()
    {
        m_Body = GetComponent<Rigidbody2D>();
        m_Body.freezeRotation = true;

        SetObjectLookat(m_LowerBody, m_LowerLookat);
        SetObjectLookat(m_UpperBody, m_UpperLookat);
    }

    /*----------------------------------------------------------------------------*/
    void Update()
    {
        CheckForSpawnPoint();

        {
            Vector2 current = transform.TransformDirection(Device.LeftStick.Vector);
            current.Normalize();

            if (current != Vector2.zero) {
                m_LowerLookat = current;
            }

            // Adjust lower body to left stick vector
            Debug.DrawRay(transform.position, m_LowerLookat, Color.green);
            SetObjectLookat(m_LowerBody, m_LowerLookat);
        }
        {
            Vector2 current = transform.TransformDirection(Device.RightStick.Vector);
            current.Normalize();

            if (current != Vector2.zero) {
                m_UpperLookat = current;
            }

            // Adjust upper body to right stick vector
            Debug.DrawRay(transform.position, m_UpperLookat, Color.red);
            SetObjectLookat(m_UpperBody, m_UpperLookat);
        }

        m_Body.AddForce(Device.LeftStick.Vector * 80.0f);
        AdjustBodyVelocity(0.75f);
    }

    ////////////////////////////////////////////////////////////////////////////////
    // Privates:
    ////////////////////////////////////////////////////////////////////////////////
    /*----------------------------------------------------------------------------*/
    private void CheckForSpawnPoint()
    {
        if (CurrentState == State.Sleeping) {
            foreach (GameObject go in GameObject.FindGameObjectsWithTag("Respawn"))
            {
                SpawnPoint spawn = go.GetComponent<SpawnPoint>();
                if (ID == spawn.m_InitialPlayerID)
                {
                    transform.position = new Vector3(
                        go.transform.position.x,
                        go.transform.position.y,
                        transform.position.z
                    );

                    CurrentState = State.Idle;
                }
            }
        }
    }

    /*----------------------------------------------------------------------------*/
    private void AdjustBodyVelocity(float factor)
    {
        m_Body.velocity = new Vector2(
            m_Body.velocity.x * factor,
            m_Body.velocity.y * factor
        );
    }

    /*----------------------------------------------------------------------------*/
    private void SetObjectLookat(GameObject target, Vector2 lookat)
    {
        float angle = (Mathf.Atan2(lookat.y, lookat.x) * Mathf.Rad2Deg) + 90.0f;
        target.transform.eulerAngles = new Vector3(0, 0, angle);
    }
}