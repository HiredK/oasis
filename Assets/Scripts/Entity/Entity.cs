﻿using UnityEngine;
using System.Collections;

public class Entity : MonoBehaviour
{
    ////////////////////////////////////////////////////////////////////////////////
    // Members:
    ////////////////////////////////////////////////////////////////////////////////
    /*----------------------------------------------------------------------------*/
    public enum State
    {
        Sleeping, // When the entity is not activated
        Idle,     // When the entity is not moving
        Moving    // When the object is moving
    };

    public State CurrentState { get; set; }

    ////////////////////////////////////////////////////////////////////////////////
    // Callbacks:
    ////////////////////////////////////////////////////////////////////////////////
    /*----------------------------------------------------------------------------*/
    void Start()
    {
        CurrentState = State.Sleeping;
    }
}