﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using InControl;

public class GameManager : MonoBehaviour
{
    ////////////////////////////////////////////////////////////////////////////////
    // Members:
    ////////////////////////////////////////////////////////////////////////////////
    /*----------------------------------------------------------------------------*/
    private static GameManager INSTANCE__ = null;
    private const int MAX_PLAYERS__ = 4;

    public string m_StartingSceneName;

    public GameObject m_SplashScreen;
    public GameObject m_PlayerPrefab;

    List<Player> m_players = new List<Player>(MAX_PLAYERS__);

    ////////////////////////////////////////////////////////////////////////////////
    // Callbacks:
    ////////////////////////////////////////////////////////////////////////////////
    /*----------------------------------------------------------------------------*/
    void Awake()
    {
        Debug.Log("Awake GameManager");
        if (INSTANCE__ == null)
        {
            INSTANCE__ = this;
            DontDestroyOnLoad(INSTANCE__.gameObject);
            if (!InitGame())
            {
                Debug.LogError("Unable to init game!");
            }
        }
        else
        {
            Debug.LogError("GameManager already exists!");
        }
    }

    /*----------------------------------------------------------------------------*/
    void Start()
    {
        InputManager.OnDeviceDetached += OnDeviceDetached;
    }

    /*----------------------------------------------------------------------------*/
    void Update()
    {
        // get current active device
        InputDevice active_device = InputManager.ActiveDevice;

        // check if device is active
        if (JoinButtonWasPressedOnDevice(active_device)) {
            if (FindPlayerUsingDevice(active_device) == null)
            {
                CreatePlayer(active_device);
            }
        }
    }

    /*----------------------------------------------------------------------------*/
    void OnDeviceDetached(InputDevice input_device)
    {
        Player player = FindPlayerUsingDevice(input_device);
        if (player != null)
        {
            RemovePlayer(player);
        }
    }

    ////////////////////////////////////////////////////////////////////////////////
    // Privates:
    ////////////////////////////////////////////////////////////////////////////////
    /*----------------------------------------------------------------------------*/
    bool InitGame()
    {
        GameObject.Instantiate(m_SplashScreen);

        /*if (Application.CanStreamedLevelBeLoaded(m_StartingSceneName))
        {
            Debug.Log("Starting scene " + m_StartingSceneName);
            SceneManager.LoadScene(m_StartingSceneName);
        }
        else
        {
            Debug.LogError("Unable to start scene!");
        }

        return true;*/

        return true;
    }

    /*----------------------------------------------------------------------------*/
    // Check if an action has been performed by a given input device.
    private bool JoinButtonWasPressedOnDevice(InputDevice input_device)
    {
        return (
            input_device.Action1.WasPressed ||
            input_device.Action2.WasPressed ||
            input_device.Action3.WasPressed ||
            input_device.Action4.WasPressed
        );
    }

    /*----------------------------------------------------------------------------*/
    // Retrieves a player instance from a given input device,
    // returns null if not found.
    private Player FindPlayerUsingDevice(InputDevice input_device)
    {
        for (int i = 0; i < m_players.Count; ++i)
        {
            Player player = m_players[i];
            if (player.Device == input_device)
            {
                return player;
            }
        }

        return null;
    }

    /*----------------------------------------------------------------------------*/
    // Instantiate a new player and assigns the given input device,
    // returns null if too many players are connected.
    private Player CreatePlayer(InputDevice input_device)
    {
        if (m_players.Count < MAX_PLAYERS__)
        {
            Debug.Log("A new player join the game!");

            // instantiate player object
            GameObject player_object = (GameObject)Instantiate(m_PlayerPrefab);

            // retrieve player component
            Player component = player_object.GetComponent<Player>();

            component.Device = input_device;
            component.ID = m_players.Count;

            m_players.Add(component);

            return component;
        }

        return null;
    }

    /*----------------------------------------------------------------------------*/
    // Removes a given player instance from the list and destroy it.
    private void RemovePlayer(Player player)
    {
        Debug.Log("A player has left game!");

        m_players.Remove(player);
        player.Device = null;
        Destroy(player.gameObject);
    }

    ////////////////////////////////////////////////////////////////////////////////
    // Accessors:
    ////////////////////////////////////////////////////////////////////////////////
    /*----------------------------------------------------------------------------*/
    public static GameManager GetInstance()
    {
        return INSTANCE__;
    }
    /*----------------------------------------------------------------------------*/
    public Player GetPlayerAt(int index)
    {
        return m_players[index];
    }
    /*----------------------------------------------------------------------------*/
    public int GetCurrentPlayerCount()
    {
        return m_players.Count;
    }
    /*----------------------------------------------------------------------------*/
    public static int GetMaxPlayers()
    {
        return MAX_PLAYERS__;
    }
}