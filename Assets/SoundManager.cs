﻿using UnityEngine;
using System.Collections;

public class SoundManager : MonoBehaviour 
{

	private static SoundManager INSTANCE__ = null;


	private AudioSource m_AudioSource;


	public AudioClip[] m_SoundList;
	public AudioClip[] m_MusicList;


	void Awake()
    {
        Debug.Log("Awake GameManager");
        if (INSTANCE__ == null)
        {
            INSTANCE__ = this;
            DontDestroyOnLoad(INSTANCE__.gameObject);
            if (!InitGame())
            {
                Debug.LogError("Unable to init game!");
            }
        }
        else
        {
            Debug.LogError("GameManager already exists!");
        }
    }




	bool InitGame()
    {
		m_AudioSource = GetComponent<AudioSource>();    	

        return true;
    }


    public void PlaySound(string _sound)
    {
		AudioClip clip = new AudioClip();

		for(int i = 0 ; i < m_SoundList.Length; i++)
		{
			if(_sound == m_SoundList[i].name)
			{
				clip = m_SoundList[i];
			}
		}

		if(clip != null)
		{
			m_AudioSource.PlayOneShot(clip);
		}	
    }

	void Update () 
	{
		if(Input.GetKeyDown(KeyCode.P))
		{
			PlaySound("fireball2");
		}
	}
}
